import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'

import Users from '../views/users/Overview.vue'
import UserAdd from '../views/users/Add.vue'


import idsrvAuth from '../idsrvAuth'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    component: About
  },
  {
    path: '/admin/users',
    component: Users
  },
  {
    path: '/admin/users/add',
    component: UserAdd
  }
].map(rr => {
  if (rr.path.startsWith("/admin")) {
    // @ts-ignore
    rr.meta = {
      authName: idsrvAuth.authName
    }
  }
  return rr
})

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

idsrvAuth.useRouter(router)

export default router
