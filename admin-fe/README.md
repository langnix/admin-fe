# v3-ts

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## User
martin.harm.42+001@gmail.com Geheim68UndAus
martin.harm.42+002@gmail.com Geheim68UndAus

